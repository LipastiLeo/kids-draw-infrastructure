CREATE DATABASE IF NOT EXISTS kidsdraw;
USE kidsdraw;

/* DROP TABLE IF EXISTS drawings CASCADE; */
/* DROP TABLE IF EXISTS friendships CASCADE; */
/* DROP TABLE IF EXISTS subusers CASCADE; */
/* DROP TABLE IF EXISTS users CASCADE; */

/* Users */
CREATE TABLE IF NOT EXISTS users(
  id BIGINT UNSIGNED AUTO_INCREMENT,
  username VARCHAR(255) NOT NULL,
  email VARCHAR(255) NOT NULL,
  password VARCHAR(64) NOT NULL,
  active BOOLEAN DEFAULT 1,
  reg_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/* Subusers - These are created by users */
CREATE TABLE IF NOT EXISTS subusers(
  id BIGINT UNSIGNED AUTO_INCREMENT,
  username VARCHAR(255) NOT NULL,
  password VARCHAR(255) DEFAULT NULL,
  isadmin BOOLEAN DEFAULT 0,
  icon TINYINT,
  friendshipkey VARCHAR(125) UNIQUE,
  friendshippassword VARCHAR(255) DEFAULT NULL,
  userid BIGINT UNSIGNED NOT NULL,
  active BOOLEAN DEFAULT 1,
  reg_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  FOREIGN KEY (userid) REFERENCES users(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/* Friendships - friend connections between subusers*/
/* uniqcode reduces queries and new entries if ANY friendships entry exists between these 2 subusers.*/
/* uniqcode is sort(['U'+receiverid,'U'+requesterid]) in javascript.*/
/* simply put: uniqcode stays always same between 2 subuser-ids regardless who triggered original invite.*/
CREATE TABLE IF NOT EXISTS friendships(
  id BIGINT UNSIGNED AUTO_INCREMENT,
  receiverid BIGINT UNSIGNED NOT NULL,
  requesterid BIGINT UNSIGNED NOT NULL,
  uniqcode VARCHAR(255) UNIQUE,
  accepted BOOLEAN DEFAULT 0,
  active BOOLEAN DEFAULT 1,
  reg_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  FOREIGN KEY (receiverid) REFERENCES subusers(id),
  FOREIGN KEY (requesterid) REFERENCES subusers(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/* Drawing Links - sent drawing to a friend*/
/* author - image sender/author*/
/* uniqcode - foreign key to friendships, reveals relationship.*/
/* link - image link*/
/* active - receiver's admin can set active true. Unfriending also sets drawings false.*/
CREATE TABLE IF NOT EXISTS drawings(
  id BIGINT UNSIGNED AUTO_INCREMENT,
  authorid BIGINT UNSIGNED NOT NULL,
  uniqcode VARCHAR(255) NOT NULL,
  link VARCHAR(255) UNIQUE,
  active BOOLEAN DEFAULT 0,
  reg_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  FOREIGN KEY (authorid) REFERENCES subusers(id),
  FOREIGN KEY (uniqcode) REFERENCES friendships(uniqcode)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/* Permissions. Run build if empty */
