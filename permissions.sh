#!/bin/sh
export $(cat .env | xargs)
command >> init.sql
echo "DROP USER IF EXISTS '$DB_USER';" >> init.sql
echo "DROP USER IF EXISTS '$DB_USER'@'$SERVER_IP';" >> init.sql
echo "CREATE USER '$DB_USER'@'$SERVER_IP' IDENTIFIED BY '$DB_PASSWORD';" >> init.sql
echo "GRANT INSERT, SELECT, UPDATE ON kidsdraw.* TO '$DB_USER'@'$SERVER_IP';" >> init.sql
echo "DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('localhost', '127.0.0.1', '::1');" >> init.sql
echo "ALTER USER 'root'@'localhost' IDENTIFIED BY '$DB_ROOT_PASSWORD';" >> init.sql
echo "SET GLOBAL max_connect_errors=6;" >> init.sql
echo "SET GLOBAL max_connections=100;" >> init.sql
echo "SET GLOBAL max_user_connections=50;" >> init.sql
echo "FLUSH PRIVILEGES;" >> init.sql