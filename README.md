# Kids-Draw-infrastructure

Simple development set up which allows to run all projects from the single entry point, in this case react (frontend) and node server (backend).

# Docker

If you are new to docker environment read more at https://docs.docker.com.<br />
Once you can run docker run hello-world without 'sudo' you are good to go.<br />

after cloning repositories your folder structure should look like this:<br />
kids-draw-infrastructure/<br />
kids-draw-server/<br />
kids-draw/<br />

Then Create your .env here. See details below.<br />

You will also need to use following command to create external volume for database:<br />
docker volume create --name=KD_database

## first run:<br />
npm run build (this creates permissions to init.sql based on your .env)<br />
npm run start (or docker-compose up -d)<br />

## after:
npm run start (or docker-compose up -d)<br />
npm run stop (or docker-compose down)<br />

## useful docker commands:<br />
docker ps -a<br />
docker logs container_id<br />
docker exec -it container_id bash<br />
docker volume rm volume_name (if you need to remove mysql volume)<br />
docker-compose up -d --force-recreate --build<br />

# ENV

For security create your .env file in this folder and add your envs:<br />
DB_VOLUME_ID=replace_this_with_a_volumename_001<br />
DB_USER=db_username_other_than_root<br />
DB_PASSWORD=db_password_for_that_username<br />
DB_ROOT_PASSWORD=root_password_make_it_difficult<br />
also whitelist ips (for example):<br />
REACT_IP=172.23.1.1<br />
SERVER_IP=172.23.1.2<br />
DB_IP=172.23.1.3<br />
WHITELIST_IP_RANGE=172.23.0.0/16<br />

## stdin_open: true

fixes issue in react and docker with the used react version. stdin_open: true in docker-compose.yml makes it so react doesnt exit with code 0.
